package spring.json.endpoints.checkpoint.Service;

import com.google.gson.Gson;
import org.junit.Test;
import spring.json.endpoints.checkpoint.Domain.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import static org.junit.Assert.assertEquals;

public class ActivityServiceTest {

    private ActivityService activityService = new ActivityService();
    private Gson gson = new Gson();

    @Test
    public void makeDetailJsonTest() throws FileNotFoundException {
        File jsonFile = new File("src/test/resources/detailed_activity.json");
        Scanner myReader = new Scanner(jsonFile);
        StringBuilder sb = new StringBuilder();
        while (myReader.hasNextLine()) {
            sb.append(myReader.nextLine());
        }
        myReader.close();

        SimpleActivity simpleActivity = gson.fromJson(sb.toString(), SimpleActivity.class);
        List<Response> responseList = activityService.makeDetailJson(simpleActivity);
        assertEquals(2, responseList.size());
        assertEquals(1467L, ((ResponseUserDetailed)responseList.get(0)).getUserId());
        assertEquals("someuser", ((ResponseUserDetailed)responseList.get(0)).getUserName());
        assertEquals("personal@example.com", ((ResponseUserDetailed)responseList.get(0)).getEmail());
        assertEquals("2017-04-02 01:32", ((ResponseUserDetailed)responseList.get(0)).getDate());
        assertEquals("Just went snowboarding today!", ((ResponseUserDetailed)responseList.get(0)).getStatusText());

        assertEquals(98732L, ((ResponseUserDetailed)responseList.get(1)).getUserId());
        assertEquals("otheruser", ((ResponseUserDetailed)responseList.get(1)).getUserName());
        assertEquals("otherprimary@example.com", ((ResponseUserDetailed)responseList.get(1)).getEmail());
        assertEquals("2017-04-02 01:32", ((ResponseUserDetailed)responseList.get(1)).getDate());
        assertEquals("Great times!", ((ResponseUserDetailed)responseList.get(1)).getStatusText());
    }

    @Test
    public void makeCompactJsonTest() throws FileNotFoundException {
        File jsonFile = new File("src/test/resources/detailed_activity.json");
        Scanner myReader = new Scanner(jsonFile);
        StringBuilder sb = new StringBuilder();
        while (myReader.hasNextLine()) {
            sb.append(myReader.nextLine());
        }
        myReader.close();

        SimpleActivity simpleActivity = gson.fromJson(sb.toString(), SimpleActivity.class);
        List<Response> responseList = activityService.makeCompactJson(simpleActivity);
        assertEquals(2, responseList.size());
        assertEquals("someuser", ((ResponseUser)responseList.get(0)).getUserName());
        assertEquals("Just went snowboarding today!", ((ResponseUser)responseList.get(0)).getStatusText());
        assertEquals("2017-04-02 01:32", ((ResponseUser)responseList.get(0)).getDate());

        assertEquals("otheruser", ((ResponseUser)responseList.get(1)).getUserName());
        assertEquals("Great times!", ((ResponseUser)responseList.get(1)).getStatusText());
        assertEquals("2017-04-02 01:32", ((ResponseUser)responseList.get(1)).getDate());
    }

}
