package spring.json.endpoints.checkpoint.Controller;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.File;
import java.util.Scanner;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes={com.example.Application.class})
@AutoConfigureMockMvc
public class ActivityControllerTest {

    @Autowired
    MockMvc mvc;
    private Gson gson = new Gson();

    @Test
    void testPostDetailedActivitesSimplify() throws Exception{
        File jsonFile = new File("src/test/resources/detailed_activity.json");
        Scanner myReader = new Scanner(jsonFile);
        StringBuilder sb = new StringBuilder();
        while (myReader.hasNextLine()) {
            sb.append(myReader.nextLine());
        }
        myReader.close();


        File detailedResponseJsonFile = new File("src/test/resources/post_activity_detailed_response.json");
        Scanner myReader1 = new Scanner(detailedResponseJsonFile);
        StringBuilder sb1 = new StringBuilder();
        while (myReader1.hasNextLine()) {
            sb1.append(myReader1.nextLine());
        }
        myReader1.close();

        RequestBuilder request = MockMvcRequestBuilders.post("/activities/simplify")
                .header("Accept", "application/vnd.galvanize.detailed+json")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb.toString());

        String expectedString = gson.toJson(sb1.toString()).replace("\\", "");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(expectedString.substring(1, expectedString.length() - 1)));
    }

    @Test
    void testPostCompactActivitesSimplify() throws Exception{
        File jsonFile = new File("src/test/resources/detailed_activity.json");
        Scanner myReader = new Scanner(jsonFile);
        StringBuilder sb = new StringBuilder();
        while (myReader.hasNextLine()) {
            sb.append(myReader.nextLine());
        }
        myReader.close();


        File detailedResponseJsonFile = new File("src/test/resources/post_activity_compact_response.json");
        Scanner myReader1 = new Scanner(detailedResponseJsonFile);
        StringBuilder sb1 = new StringBuilder();
        while (myReader1.hasNextLine()) {
            sb1.append(myReader1.nextLine());
        }
        myReader1.close();

        RequestBuilder request = MockMvcRequestBuilders.post("/activities/simplify")
                .header("Accept", "application/vnd.galvanize.compact+json")
                .contentType(MediaType.APPLICATION_JSON)
                .content(sb.toString());

        String expectedString = gson.toJson(sb1.toString()).replace("\\", "");

        this.mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(expectedString.substring(1, expectedString.length() - 1)));
    }
}
