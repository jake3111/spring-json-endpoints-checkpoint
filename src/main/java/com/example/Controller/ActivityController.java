package com.example.Controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import spring.json.endpoints.checkpoint.Domain.Response;
import spring.json.endpoints.checkpoint.Domain.SimpleActivity;
import spring.json.endpoints.checkpoint.Service.ActivityService;

import java.util.List;

@RestController
public class ActivityController {
    ActivityService activityService = new ActivityService();

    @PostMapping("/activities/simplify")
    public List<Response> simpleActivity(@RequestBody SimpleActivity simpleActivity, @RequestHeader("Accept") String requestHeader) {
        if (requestHeader != null && requestHeader.equals("application/vnd.galvanize.detailed+json")) {
            return activityService.makeDetailJson(simpleActivity);
        } else if (requestHeader != null && requestHeader.equals("application/vnd.galvanize.compact+json")){
            return activityService.makeCompactJson(simpleActivity);
        } else {
            return null;
        }
    }

}
