package spring.json.endpoints.checkpoint.Service;

import spring.json.endpoints.checkpoint.Domain.*;

import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class ActivityService {

    public List<Response> makeDetailJson(SimpleActivity simpleActivity){
        List<Response> responseUserDetailedList = new Stack<>();
        for (Activity activity : simpleActivity.getActivities()) {
            ResponseUserDetailed responseUserDetailed = new ResponseUserDetailed();
            responseUserDetailed.setUserId(activity.getUser().getId());
            responseUserDetailed.setUserName(activity.getUser().getUsername());
            String primaryEmail = activity.getUser().getEmails()
                    .stream()
                    .filter(Email::isPrimary)
                    .collect(Collectors.toList())
                    .get(0)
                    .getAddress();
            responseUserDetailed.setEmail(primaryEmail);
            responseUserDetailed.setDate(activity.getStatus().getDate());
            responseUserDetailed.setStatusText(activity.getStatus().getText());
            responseUserDetailedList.add(responseUserDetailed);
        }
        return responseUserDetailedList;
    }

    public List<Response> makeCompactJson(SimpleActivity simpleActivity){
        List<Response> responseUserDetailedList = new Stack<>();
        for (Activity activity : simpleActivity.getActivities()) {
            ResponseUser responseUser = new ResponseUser();
            responseUser.setUserName(activity.getUser().getUsername());
            responseUser.setDate(activity.getStatus().getDate());
            responseUser.setStatusText(activity.getStatus().getText());
            responseUserDetailedList.add(responseUser);
        }
        return responseUserDetailedList;

    }
}
