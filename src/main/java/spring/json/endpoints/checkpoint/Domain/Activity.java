package spring.json.endpoints.checkpoint.Domain;

public class Activity {

    private User user;
    private Status status;

    public User getUser() {
        return user;
    }

    public Status getStatus() {
        return status;
    }
}
