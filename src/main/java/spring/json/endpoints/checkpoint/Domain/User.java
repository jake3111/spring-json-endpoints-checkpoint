package spring.json.endpoints.checkpoint.Domain;

import java.util.List;

public class User {

    private long id;
    private String username;
    private List<Email> emails;

    public long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public List<Email> getEmails() {
        return emails;
    }
}
