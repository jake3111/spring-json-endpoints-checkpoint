package spring.json.endpoints.checkpoint.Domain;

import java.util.Date;

public class Status {

    private String text;
    private String date;

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }
}
