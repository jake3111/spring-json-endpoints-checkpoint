package spring.json.endpoints.checkpoint.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseUser implements Response {

    private String userName;
    private String date;
    private String statusText;

    @JsonProperty("user")
    public String getUserName() {
        return userName;
    }

    public String getDate() {
        return date;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}
