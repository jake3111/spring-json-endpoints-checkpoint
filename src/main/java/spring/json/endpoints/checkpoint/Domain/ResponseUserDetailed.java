package spring.json.endpoints.checkpoint.Domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ResponseUserDetailed implements Response {

    private long userId;
    private String userName;
    private String email;
    private String date;
    private String statusText;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @JsonProperty("user")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }
}
