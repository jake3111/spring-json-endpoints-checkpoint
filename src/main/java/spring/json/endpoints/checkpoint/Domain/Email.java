package spring.json.endpoints.checkpoint.Domain;

public class Email {

    private long id;
    private String address;
    private boolean primary;

    public String getAddress() {
        return address;
    }

    public boolean isPrimary() {
        return primary;
    }
}
